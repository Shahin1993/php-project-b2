<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <title> project bootstrap</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/screen.css">
    <style>#commentForm{width:500px;}#commentForm label{width:250px;}#commentForm label.error,#commentForm input.submit{margin-left:253px;}#myForm{width:670px;}#myForm label.error{margin-left:10px;width:auto;display:inline;}#newsletter_topics label.error{display:none;margin-left:103px;}</style>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 90%;
            margin: auto;
            height: 300px;
        }
    </style>

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">


<nav class="navbar navbar-inverse">

    <ul class="nav navbar-nav">
        <li><a href="#">Home</a></li>
        <li><a href="#">contact</a></li>
        <li><a href="auth/register.php">Sign Up</a></li>
        <li><a href="auth/login.php">Login</a></li>

    </ul>

    <form class=" navbar-form navbar-right">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="search"/>
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="glyphicon glyphicon-search"></i>

                </button>
            </div>
        </div>
    </form>

</nav>
<div id="slidde">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="image/4.jpg" alt="Apon Manus"  />
                <div class="carousel-caption">
                    <h3>Apon Maus</h3>
                    <p>Apon manus dukkho dile jay ki tare vola</p>
                </div>

            </div>
            <div class="item">
                <img src="image/2.jpg" alt="Por manusi"  />
                <div class="carousel-caption">
                    <h3>Por manusi</h3>
                    <p>Pormanusi kothay jaow</p>
                </div>
            </div>
            <div class="item">
                <img src="image/3.jpg" alt="third person"  />
                <div class="carousel-caption">
                    <h3>Third person</h3>
                    <p>third person is the best </p>
                </div>
            </div>

        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" ></span>
            <span class="sr-only">Next</span>
        </a>

    </div>

</div>
<div class="well">
    <div class="container-fluid">
        <div class="page-header text-center">
            <h1>Management</h1>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                    <img src="image/4444.jpg" class="img-responsive img-circle" alt="image21" />
                    <div class="caption text-center">
                        <h4>President</h4>
                        <p>
                            One of the things I have found myself needing over and
                            over again as I design websites is "dummy text" or
                            "filler text", that looks like real content, that I
                            can use to fill up a page so it gives the client
                        </p>
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#mymodal2">
                            View all information....
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="thumbnail">
                    <img src="image/322.jpg" class="img-responsive img-circle" alt="imge31"/>
                    <div class="caption text-center">
                        <h4>Chancellor</h4>
                        <p>
                            One of the things I have found myself needing over and
                            over again as I design websites is "dummy text" or
                            "filler text", that looks like real content, that I
                            can use to fill up a page so it gives the client
                        </p>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mymodal3">view all information....</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <img src="image/41.jpg" class="img-responsive img-circle" alt="image41" />
                    <div class="caption text-center">
                        <h4>Vice Chancellor</h4>
                        <p>
                            One of the things I have found myself needing over and
                            over again as I design websites is "dummy text" or
                            "filler text", that looks like real content, that I
                            can use to fill up a page so it gives the client
                        </p>
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#mymodal2">view all information...</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mymodal2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Md Durjoy Chowdhury</h3>
            </div>
            <div class="modal-body">
                <p>
                    One of the things I have found myself needing over and
                    over again as I design websites is "dummy text" or
                    "filler text", that looks like real content, that I
                    can use to fill up a page so it gives the client
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mymodal3">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"">&times;</button>
                <h4>Tnisha khanom</h4>
            </div>
            <div class="modal-body">
                <p>
                    One of the things I have found myself needing over and
                    over again as I design websites is "dummy text" or
                    "filler text", that looks like real content, that I
                    can use to fill up a page so it gives the client
                </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div><!----modal-->



<div class="cotainer-fluid text-center">
    <div class="page-header">
        <h2>our service</h2>
    </div>
    <div class="btn-group">
        <a href="#mobile" class="btn btn-info btn-lg" data-toggle="tab">mobile</a>
        <a href="#tablat" class="btn btn-warning btn-lg" data-toggle="tab">tablat</a>
        <a href="#laptop" class="btn btn-info btn-lg" data-toggle="tab">laptop</a>

    </div>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="mobile">
            <div class="tab-header">
                <h3>gallary</h3>
            </div>
            <div class="row-fluid">
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/55.jpg" alt=""/>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/55.jpg" alt="" />
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/55.jpg" alt="" />
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/55.jpg" alt="" />
                    </a>
                </div>
            </div>

        </div><!--tab pane-->

        <div class="tab-pane fade" id="tablat">
            <div class="tab-header">
                <h3>gallary</h3>
            </div>
            <div class="row-fluid">
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/21.png" alt=""/>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/21.png" alt="" />
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/21.png" alt="" />
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/21.png" alt="" />
                    </a>
                </div>
            </div>

        </div><!--tab pane-->
        <div class="tab-pane fade" id="laptop">
            <div class="tab-header">
                <h3>gallary</h3>
            </div>
            <div class="row-fluid">
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/31.png" alt=""/>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/31.png" alt="" />
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/31.png" alt="" />
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="" class="thumbnail">
                        <img src="image/31.png" alt="" />
                    </a>
                </div>
            </div>

        </div><!--tab pane-->
    </div>


</div>


<!--feedback-->
<div class="well feedback">
    <div class="container-fluid">
        <div class="pag-header">
            <h1>feedback<small>clinet service</small></h1>
        </div>
        <div class="row-fluid">
            <div class="col-md-4">
                <blockquote>
                    <p>
                        One of the things I have found myself needing over and
                        over again as I design websites is "dummy text" or
                        ike real content, that I

                    </p>
                    <footer>sadik</footer>
                </blockquote>
            </div>
            <div class="col-md-4">
                <blockquote>
                    <p>
                        One of the things I have found myself needing over and
                        over again as I design websites is "dummy text" or
                        ike real content, that I

                    </p>
                    <footer>sadik</footer>
                </blockquote>
            </div>
            <div class="col-md-4">
                <blockquote>
                    <p>
                        One of the things I have found myself needing over and
                        over again as I design websites is "dummy text" or
                        ike real content, that I

                    </p>
                    <footer>sadik</footer>
                </blockquote>
            </div>
        </div>
    </div><!---feedback-->
</div>




<div align="center" class="well" id="footer">
    <footer>copy right &copy; Shahin</footer>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js"></script>
</body>
</html>
