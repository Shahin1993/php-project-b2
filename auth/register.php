 <html>
 <head>
     <title>Welcome our site</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
     <link rel="stylesheet" href="../css/style.css" type="text/css" />
 </head>
 <body>
<div class="register">
 <form  id="myForm" class="form-horizotal" action="">
     <fieldset>
         <div class="input-group">
             <label class="control-label" for="name">Name:</label>
             <div>
                 <input type="text" id="name" class="form-control" name="name" placeholder="enter your name"/>
             </div>
             <div class="input-group">
                 <label class="control-label" for="email">Email:</label>
                 <div>
                     <input type="email" class="form-control" id="email" name="email" placeholder="eamil..."/>

                 </div>
                 <div class="input-group">
                     <label for="pass" >Pssword:</label>
                     <div>
                     <input type="password" class="form-control" id="pass" name="pass"/>
                     </div>

                     <div class="input-group">
                     <label for="cnfpass">Conform Password:</label>
                         <div>
                     <input type="password" class="form-control" id="cnfpass"  name="cnfpass" placeholder="againg..."/>
                         </div>
                 <button type="submit" class="btn btn-info btn-lg" id="btn">Save</button>

                     </div>
                 </div>
             </div>

         </div>
     </fieldset>
 </form>

</div>



 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


 <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js"></script>
 <script>
     $(document).ready(function(){
         $("#myForm").validate({
             rules: {
                 name: "required",
                 email:
                     {
                         required:true,
                     },
                 pass:{
                     required:true,
                     minlength:6
                 },
                 cnfpass:{
                     required:true,
                     minlength:6,
                     equalTo:"#pass"
                 }
             },
             messages:{
                 name:"please enter your valide name",
                 email:"Please enter your valide email"
             }
         });



     });

 </script>

 </body>
 </html>